package com.example.agenda;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private EditText et_nombre, et_datos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et_nombre = (EditText)findViewById(R.id.idTxtNom);
        et_datos = (EditText)findViewById(R.id.txtDatos);
        /*et_correo = (EditText)findViewById(R.id.idTxtCorreo);
        et_telefono = (EditText)findViewById(R.id.idTxtTel);*/


    }

    public void Agregar(View view){

        String nombre = et_nombre.getText().toString();
        String datos = et_datos.getText().toString();
        //ArrayList<String> datos = new ArrayList<>();
       // String datos[] = {correo, telefono};

        SharedPreferences preferencias = getSharedPreferences("agenda", Context.MODE_PRIVATE);
        SharedPreferences.Editor obj_editor = preferencias.edit();
        obj_editor.putString(nombre, datos);
        obj_editor.commit();

        Toast.makeText(this, "El contacto a sido registrado", Toast.LENGTH_SHORT).show();
        et_nombre.setText(null);
        et_datos.setText(null);
    }

    public void Buscar(View view){

        String nombre = et_nombre.getText().toString();
        SharedPreferences preferencias = getSharedPreferences("agenda", Context.MODE_PRIVATE);
        String datos = preferencias.getString(nombre, "");

        if(datos.length() == 0){

            Toast.makeText(this, "No se encontro registro del contacto", Toast.LENGTH_SHORT).show();

        }else {
            et_datos.setText(datos);

        }
    }
}